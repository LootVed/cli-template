package cfg

import (
	"log"

	"gopkg.in/yaml.v3"
)

var Cfg struct {
	Host  string
	Port  string
	Topic string

	Push struct {
		FromFile bool
	}
}

func Export() []byte {
	data, err := yaml.Marshal(&Cfg)
	if err != nil {
		log.Fatalln(err)
	}
	return data
}
