package cfg

import "github.com/spf13/viper"

func InitViper() error {
	viper.SetConfigName("template")
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")
	viper.AddConfigPath("config")
	return viper.ReadInConfig()
}
