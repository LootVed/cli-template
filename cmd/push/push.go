package push

import (
	"log"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	. "gitlab.com/lootved/cli-template/cfg"
)

func Init() *cobra.Command {
	var cmd = &cobra.Command{
		Use:   "push",
		Short: "Push messages to the topic",
		Long: "Push message from the command line arguments or a file.\n" +
			"Each line of the file, or command line argument will be considered as a different message",
		Run:  pushMessages,
		Args: cobra.MinimumNArgs(1),
	}

	cmd.PersistentFlags().BoolVarP(&Cfg.Push.FromFile, "fromfile", "f",
		false, "consider argument as file to push message from")

	viper.BindPFlag("push.fromfile", cmd.Flags().Lookup("fromfile"))

	return cmd
}

func pushMessages(cmd *cobra.Command, args []string) {
	server := Cfg.Host + ":" + Cfg.Port
	if Cfg.Push.FromFile {
		log.Println("Argument is treated as a file")
		if len(args) > 1 {
			log.Println("[WARN] ignoring all arguments except the first one")
		}
	} else {
		log.Println("Arguments are treated as a message")
	}
	log.Println("Pushing messages to topic", "'"+Cfg.Topic+"'", "on", "'"+server+"'")
}
